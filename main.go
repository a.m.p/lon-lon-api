package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
	"database/sql"
	"os"

	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	"github.com/gorilla/context"
	"github.com/mitchellh/mapstructure"
	_ "github.com/lib/pq"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

type User struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type Vendor struct {
	Owner string `json:"owner"`
	BusiName string `json:"name"`
	Username string `json:"username"`
	Password string `json:"password"`
	Phone string `json:"phone"`
	Street string `json:"street"`
	City string `json:"city"`
	State string `json:"state"`
	Zip string `json:"zipcode"`
	Catagory string `json:"category"`
}

type Item struct {
	Id int `json:"id"`
	Name string `json:"item"`
        Descr string `json:"description"`
	Variants string `json:"variants"`
        Img0 string `json:"img0"`
        Img1 string `json:"img1"`
        Img2 string `json:"img2"`
	ActualPrice float64 `json:"actual_price"`
	SellingPrice float64 `json:"selling_price"`
        Quantity int `json:"quantity"`
	Unit string `json:"unit"`
}

type NewItem struct {
	Name string `json:"item"`
        Descr string `json:"description"`
	Variants string `json:"variants"`
        Img0 string `json:"img0"`
        Img1 string `json:"img1"`
        Img2 string `json:"img2"`
	ActualPrice float64 `json:"actual_price"`
	SellingPrice float64 `json:"selling_price"`
        Quantity int `json:"quantity"`
	Unit string `json:"unit"`
}

type Id struct {
	Id int `json:"id"`
}

type CustUser struct {
	U string `json:"username"`
}

type Images struct {
	Img0 string `json:"img0"`
	Img1 string `json:"img1"`
	Img2 string `json:"img2"`
}

type Customer struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Email string `json:"email"`
	Name string `json:"name"`
	Phone string `json:"phone"`
	Address string `json:"address"`
}

type UpdateCustomer struct {
	CID int `json:"cust_id"`
	Username string `json:"username"`
	Password string `json:"password"`
	Email string `json:"email"`
	Name string `json:"name"`
	Phone string `json:"phone"`
	Address string `json:"address"`
}

type Product struct {
	ProductId int `json:"product_id"`
	VendorId int `json:"vendor_id"`
	Name string `json:"item"`
        Descr string `json:"description"`
	Variants string `json:"variants"`
        Img0 string `json:"img0"`
        Img1 string `json:"img1"`
        Img2 string `json:"img2"`
	ActualPrice float64 `json:"actual_price"`
	SellingPrice float64 `json:"selling_price"`
        Quantity int `json:"quantity"`
	Unit string `json:"unit"`
}

type CustOrder struct {
	OrderId int `json:"order_id"`
	Pname string `json:"product_name"`
 	Price float64 `json:"price"`
	Vname string `json:"vendor_name"`
}

type VendorOrder struct {
	OrderId int `json:"order_id"`
	Pname string `json:"product_name"`
	Price float64 `json:"price"`
	Cname string `json:"cust_name"`
	Cemail string `json:"cust_email"`
	Cphone string `json:"cust_phone"`
}

type Buyer struct {
	CID int `json:"cust_id"`
	VID int `json:"vendor_id"`
	PID int `json:"product_id"`
	Pname string `json:"product_name"`
	Price float64 `json:"total"`
}

type JwtToken struct {
	Token string `json:"token"`
}

type LoggedCust struct {
	Type string `json:"type"`
	CID int `json:"cust_id"`
}

type Exception struct {
	Message string `json:"message"`
}

func loginToken( w http.ResponseWriter, req *http.Request) {
	db, err := sql.Open("postgres", os.Getenv("DATABASE_URL"))
	if(CheckError(err)){
		fmt.Println("Connected!")
	} else {
		fmt.Println(err)
	}
	defer db.Close()
	var vendorLogin User
	var user string
	var pass string
	_ = json.NewDecoder(req.Body).Decode(&vendorLogin)
	sqlStmt := `SELECT username, password FROM vendors WHERE username=$1 AND password=$2`
	row := db.QueryRow(sqlStmt, vendorLogin.Username, vendorLogin.Password)
	err = row.Scan( &user, &pass)
	if err == sql.ErrNoRows {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte(`{"message": "Username and Password are invalid"}`))
	} else {
		token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
			"username": vendorLogin.Username,
			"password": vendorLogin.Password,
		})
		tokenString, error := token.SignedString([]byte("lonlonapi"))
		if error != nil {
			fmt.Println(error)
		}
		json.NewEncoder(w).Encode(JwtToken{Token: tokenString})
	}
}

func validate(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		authorizationHeader := req.Header.Get("authorization")
		if authorizationHeader != "" {
			bearerToken := strings.Split(authorizationHeader, " ")
			if len(bearerToken) == 2 {
				token, error := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
					if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
						return nil, fmt.Errorf("There was an error")
					}
					return []byte("lonlonapi"), nil
				})
				if error != nil {
					w.WriteHeader(http.StatusBadRequest)
					w.Write([]byte(`{"message": "Cannot be validated"}`))
					return
				}
				if token.Valid {
					context.Set(req, "decoded", token.Claims)
					next(w, req)
				} else {
					w.WriteHeader(http.StatusBadRequest)
					w.Write([]byte(`{"message": "Invalid authorization token."}`))
				}
			}
		} else {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(`{"message": "Authorization header is required."}`))
		}
	})
}

func registerVendor( w http.ResponseWriter, req *http.Request) {
	db, err := sql.Open("postgres", os.Getenv("DATABASE_URL"))
	if(CheckError(err)){
		fmt.Println("Connected!")
	} else {
		fmt.Println(err)
	}
	defer db.Close()
	var vendor Vendor
	var user User
	_ = json.NewDecoder(req.Body).Decode(&vendor)

	queryStmt := fmt.Sprint("select username, password from vendors where username = '", vendor.Username, "' and password = '", vendor.Password, "'")
	regRow := db.QueryRow(queryStmt)
	err = regRow.Scan(&user.Username, &user.Password)
	if(vendor.Username == user.Username) {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(`{"message": "Username has been taken."}`))
	} else {
		insertStmt := `insert into vendors ("owner", "busi_name", "username", "password", "phone", "street", "city", "state", "zip", "category") values($1,$2,$3,$4,$5,$6,$7,$8,$9,$10)`
		_, e := db.Exec(insertStmt,
			vendor.Owner,
			vendor.BusiName,
			vendor.Username,
			vendor.Password,
			vendor.Phone,
			vendor.Street,
			vendor.City,
			vendor.State,
			vendor.Zip,
			vendor.Catagory)
		if (e != nil){
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(`{"message": "Could not register user."}`))
		} else {
			w.WriteHeader(http.StatusCreated)
			w.Write([]byte(`{"message": "New vendor has been registered."}`))
		}
	}
}

func getVendorProfile(w http.ResponseWriter, req *http.Request) {
	db, err := sql.Open("postgres", os.Getenv("DATABASE_URL"))
	CheckError(err)
	fmt.Println("Connected!")
	defer db.Close()
	decoded := context.Get(req, "decoded")
	var user User
	var vendor Vendor
	var vendorId int
	mapstructure.Decode(decoded.(jwt.MapClaims), &user)
	sqlStmt := `SELECT * FROM vendors where username = $1`
	profileRow := db.QueryRow(sqlStmt, user.Username)
	if(CheckError(err)){
		err = profileRow.Scan(&vendorId,
			&vendor.Username,
			&vendor.Password,
			&vendor.Owner,
			&vendor.BusiName,
			&vendor.Phone,
			&vendor.Street,
			&vendor.City,
			&vendor.State,
			&vendor.Zip,
			&vendor.Catagory)
		if(CheckError(err)) {
			json.NewEncoder(w).Encode(vendor)
		} else {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(`{"message": "Could not find vendor profile."}`))
		}
	} else {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{"message": "Attempted server connect has failed."}`))
	}
}

func updateVendorProfile(w http.ResponseWriter, req *http.Request) {
	db, err := sql.Open("postgres", os.Getenv("DATABASE_URL"))
	if(CheckError(err)){
		fmt.Println("Connected!")
	} else {
		fmt.Println(err)
	}
	defer db.Close()
	decoded := context.Get(req, "decoded")
	var user User
	var dbUser User
	var vendor Vendor
	var vendorId int
	mapstructure.Decode(decoded.(jwt.MapClaims), &user)
	_ = json.NewDecoder(req.Body).Decode(&vendor)
	if (user.Username != vendor.Username) {
		queryStmt := fmt.Sprint("select username, password from vendors where username = '", vendor.Username, "'")
		regRow := db.QueryRow(queryStmt)
		err = regRow.Scan(&dbUser.Username, &dbUser.Password)
		if(vendor.Username == dbUser.Username) {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(`{"message": "Username has been taken."}`))
			return
		}
	}
	_, e := db.Exec(`UPDATE vendors SET username=$1, password=$2, owner=$3, busi_name=$4, phone=$5, street=$6, city=$7, state=$8, zip=$9, category=$10 WHERE username=$11 and password=$12`,
		vendor.Username,
		vendor.Password,
		vendor.Owner,
		vendor.BusiName,
		vendor.Phone,
		vendor.Street,
		vendor.City,
		vendor.State,
		vendor.Zip,
		vendor.Catagory,
		user.Username,
		user.Password)
	if(CheckError(e)){
		sqlStmt := `SELECT * FROM vendors where username = $1`
		profileRow := db.QueryRow(sqlStmt, vendor.Username)
		err = profileRow.Scan(&vendorId,
			&vendor.Username,
			&vendor.Password,
			&vendor.Owner,
			&vendor.BusiName,
			&vendor.Phone,
			&vendor.Street,
			&vendor.City,
			&vendor.State,
			&vendor.Zip,
			&vendor.Catagory)
		if(CheckError(err)) {
			json.NewEncoder(w).Encode(vendor)
		} else {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(`{"message": "Attempted server connect has failed to obtain new profile."}`))
		}
	} else {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(`{"message": "Could not find vendor profile."}`))
	}	
}
	
func getCatalog( w http.ResponseWriter, req *http.Request) {
	db, err := sql.Open("postgres", os.Getenv("DATABASE_URL"))
	if(CheckError(err)){
		fmt.Println("Connected!")
	} else {
		fmt.Println(err)
	}
	defer db.Close()
	var user User
	decoded := context.Get(req, "decoded")
	mapstructure.Decode(decoded.(jwt.MapClaims), &user)
	sqlStmt := fmt.Sprint("select product_id, product_name, description, variants, img0, img1, img2, aprice, sprice, quantity, unit from products where vendor_id = (select vendor_id from vendors where username = '", user.Username, "' and password = '", user.Password, "')")
	rows, e := db.Query(sqlStmt)
	defer rows.Close()
	if( e == sql.ErrNoRows) {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(`{"message": "Could not find any products for vendor."}`))
	} else {
		var items []Item
		for rows.Next() {
			var i Item
			err = rows.Scan(&i.Id,
				&i.Name,
				&i.Descr,
				&i.Variants,
				&i.Img0,
				&i.Img1,
				&i.Img2,
				&i.ActualPrice,
				&i.SellingPrice,
				&i.Quantity,
				&i.Unit)
			// if err != nil {
			// 	w.WriteHeader(http.StatusInternalServerError)
			// 	w.Write([]byte(`{"message": "Failure to read data from rows"}`))
			// 	json.NewEncoder(w).Encode(i)
			// 	return
			// }
			log.Println(fmt.Sprint("product: ", i))
			items = append(items, i)
		}
		json.NewEncoder(w).Encode(items)
	}
}

func getProduct( w http.ResponseWriter, req *http.Request) {
	db, err := sql.Open("postgres", os.Getenv("DATABASE_URL"))
	if(CheckError(err)){
		fmt.Println("Connected!")
	} else {
		fmt.Println(err)
	}
	defer db.Close()
	var i Item
	var pid Id
	_ = json.NewDecoder(req.Body).Decode(&pid)
	log.Println(fmt.Sprint("Product Id: ", pid.Id))	
	sqlStmt := fmt.Sprint("select product_id, product_name, description, variants, img0, img1, img2, aprice, sprice, quantity, unit from products where product_id = '", pid.Id, "'")
	err = db.QueryRow(sqlStmt).Scan(&i.Id,
		&i.Name,
		&i.Descr,
		&i.Variants,
		&i.Img0,
		&i.Img1,
		&i.Img2,
		&i.ActualPrice,
		&i.SellingPrice,
		&i.Quantity,
		&i.Unit)
	if err == sql.ErrNoRows {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(`{"message": "Could not find item."}`))
	} else {
		json.NewEncoder(w).Encode(i)
	}
}

func addProduct( w http.ResponseWriter, req *http.Request) {
	db, err := sql.Open("postgres", os.Getenv("DATABASE_URL"))
	if(CheckError(err)){
		fmt.Println("Connected!")
	} else {
		fmt.Println(err)
	}
	defer db.Close()
	var user User
	var pid Id
	var product NewItem
	decoded := context.Get(req, "decoded")
	mapstructure.Decode(decoded.(jwt.MapClaims), &user)
	_ = json.NewDecoder(req.Body).Decode(&product)
	vendorIdSqlStmt := fmt.Sprint("select vendor_id from vendors where username = '", user.Username, "' and password = '", user.Password, "'")
	insertStmt := `insert into products ("vendor_id", "product_name", "description", "variants", "img0", "img1", "img2", "aprice", "sprice", "quantity", "unit") values($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11)`
	vendorRow := db.QueryRow(vendorIdSqlStmt)
	err = vendorRow.Scan(&pid.Id)
	if(err == nil) {
		_, e := db.Exec(insertStmt,
			pid.Id,
			product.Name,
			product.Descr,
			product.Variants,
			product.Img0,	
			product.Img1,
			product.Img2,
			product.ActualPrice,
			product.SellingPrice,
			product.Quantity,
			product.Unit)
		if (e != nil) {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(`{"message": "Failure to add product"}`))
		} else {
			w.WriteHeader(http.StatusCreated)
			w.Write([]byte(`{"message": "Added new product"}`))
		}
	} else {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{"message": "Attempted server connect has failed to obtain vendor information."}`))
	}
}

func deletePicture( url string) {
	url = strings.TrimPrefix(url, "https://lonlonbucket.s3.us-east-2.amazonaws.com/")
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String("us-east-2")},
	)
	svc := s3.New(sess)
	_, err = svc.DeleteObject(&s3.DeleteObjectInput{
		Bucket: aws.String(os.Getenv("S3_BUCKET")),
		Key: aws.String(url)})
	if err != nil {
		log.Println(err)
	}
	err = svc.WaitUntilObjectNotExists(&s3.HeadObjectInput{
		Bucket: aws.String(os.Getenv("S3_BUCKET")),
		Key:    aws.String(url)})
	if err != nil {
		log.Println(err)
	}
}

// func uploadPicture( url string) {
// 	url = strings.TrimPrefix(url, "https://lonlonbucket.s3.us-east-2.amazonaws.com/")
// 	sess, err := session.NewSession(&aws.Config{
// 		Region: aws.String("us-east-2")},
// 	)
// 	uploader := s3manager.NewUploader(sess)
// 	_, err = uploader.Upload(&s3manager.UploadInput{
// 		Bucket: bucket,
// 		Key:    filename,
// 		Body:   file,
// 	})
// 	if err != nil {
// 		return err
// 	}
// 	return nil
// }

func removeProduct( w http.ResponseWriter, req *http.Request) {
	db, err := sql.Open("postgres", os.Getenv("DATABASE_URL"))
	if(CheckError(err)){
		fmt.Println("Connected!")
	} else {
		fmt.Println(err)
	}
	defer db.Close()
	var pid Id
	var a string
	var b string
	var c string
	_ = json.NewDecoder(req.Body).Decode(&pid)
	log.Println(fmt.Sprint("Product Id: ", pid.Id))	
	sqlStmt := `SELECT img0, img1, img2 FROM products where product_id = $1`
	imageRow := db.QueryRow(sqlStmt, pid.Id)
	err = imageRow.Scan(&a, &b, &c)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{"message": "Could not delete images."}`))
		return
	} else {
		if a != "" {
			deletePicture(a)
		}
		if b != "" {
			deletePicture(b)
		}
		if c != "" {
			deletePicture(c)
		}
	}
	log.Println(fmt.Sprint("Product Id: ", pid.Id))	
	_, e := db.Exec(`delete from products where product_id=$1`, pid.Id)
	log.Println(fmt.Sprint("Error: ", e))		
	if e != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{"message": "Could not delete product."}`))
	} else {
		w.WriteHeader(http.StatusAccepted)
		w.Write([]byte(`{"message": "Product deletion successful."}`))
	}
}

func updateProduct( w http.ResponseWriter, req *http.Request) {
	db, err := sql.Open("postgres", os.Getenv("DATABASE_URL"))
	if(CheckError(err)){
		fmt.Println("Connected!")
	} else {
		fmt.Println(err)
	}
	defer db.Close()
	var user User
	var product Item
	var imgs Images
	decoded := context.Get(req, "decoded")
	mapstructure.Decode(decoded.(jwt.MapClaims), &user)	
	_ = json.NewDecoder(req.Body).Decode(&product)
	sqlStmt := `SELECT img0, img1, img2 FROM products where product_id = $1`
	imageRow := db.QueryRow(sqlStmt, product.Id)
	err = imageRow.Scan(&imgs.Img0, &imgs.Img1, &imgs.Img2)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{"message": "Could not update images."}`))
		return
	} else {
		if product.Img0 != imgs.Img0 {
			deletePicture(imgs.Img0)
		}
		if product.Img1 != imgs.Img1 {
			deletePicture(imgs.Img1)
		}
		if product.Img2 != imgs.Img2 {
			deletePicture(imgs.Img2)
		}
	}
	_, e := db.Exec(`UPDATE products SET product_name=$1, description=$2, variants=$3, img0=$4, img1=$5, img2=$6, aprice=$7, sprice=$8, quantity=$9, unit=$10 WHERE product_id=$11`,
		product.Name,
		product.Descr,
		product.Variants,
		product.Img0,
		product.Img1,
		product.Img2,			
		product.ActualPrice,
		product.SellingPrice,
		product.Quantity,
		product.Unit,
		product.Id)
	log.Println(fmt.Sprint("Error: ", e))		
	if(CheckError(e)){
		productSQLStmt := `SELECT product_name, description, variants, img0, img1, img2, aprice, sprice, quantity, unit FROM products where product_id = $1`
		productRow := db.QueryRow(productSQLStmt, product.Id)
		err = productRow.Scan(&product.Name,
			&product.Descr,
			&product.Variants,
			&product.Img0,
			&product.Img1,
			&product.Img2,			
			&product.ActualPrice,
			&product.SellingPrice,
			&product.Quantity,
			&product.Unit)
		log.Println(fmt.Sprint("Error: ", err))		
		if(CheckError(err)) {
			json.NewEncoder(w).Encode(product)
		} else {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(`{"message": "Attempted server connect has failed to obtain new profile."}`))
			return
		}
	} else {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(`{"message": "Could not find vendor profile."}`))
		return
	}
}

func getOrders( w http.ResponseWriter, req *http.Request) {
	db, err := sql.Open("postgres", os.Getenv("DATABASE_URL"))
	if(CheckError(err)){
		fmt.Println("Connected!")
	} else {
		fmt.Println(err)
	}
	defer db.Close()
	var user User
	var vID int
	decoded := context.Get(req, "decoded")
	mapstructure.Decode(decoded.(jwt.MapClaims), &user)
	queryStmt := fmt.Sprint("select vendor_id from vendors where username = '", user.Username, "' and password = '", user.Password, "'")
	err = db.QueryRow(queryStmt).Scan(&vID)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(`{"message": "Could not find vendor."}`))
		return
	}
	sqlStmt := fmt.Sprint("select order_id, product_name, total, name, email, phone from orders inner join customers on orders.cust_id = customers.cust_id where vendor_id = ", vID)
	rows, e := db.Query(sqlStmt)
	defer rows.Close()
	if( e == sql.ErrNoRows) {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(`{"message": "Could not find any products for vendor."}`))
	} else {
		var o []VendorOrder
		for rows.Next() {
			var i VendorOrder
			err = rows.Scan(&i.OrderId,
				&i.Pname,
				&i.Price,
				&i.Cname,
				&i.Cemail,
				&i.Cphone)
			// if err != nil {
			// 	w.WriteHeader(http.StatusInternalServerError)
			// 	w.Write([]byte(`{"message": "Failure to read data from rows"}`))
			// 	json.NewEncoder(w).Encode(i)
			// 	return
			// }
			log.Println(fmt.Sprint("order: ", i))
			o = append(o, i)
		}
		json.NewEncoder(w).Encode(o)
	}
}

func custLogin( w http.ResponseWriter, req *http.Request) {
	db, err := sql.Open("postgres", os.Getenv("DATABASE_URL"))
	if(CheckError(err)){
		fmt.Println("Connected!")
	} else {
		fmt.Println(err)
	}
	defer db.Close()
	var custLogin User
	var user User
	var logCust LoggedCust
	_ = json.NewDecoder(req.Body).Decode(&custLogin)
	sqlStmt := `SELECT username, password, cust_id FROM customers WHERE username=$1 AND password=$2`
	row := db.QueryRow(sqlStmt, custLogin.Username, custLogin.Password)
	err = row.Scan( &user.Username, &user.Password, &logCust.CID)
	if err == sql.ErrNoRows {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte(`{"message": "Username and Password are invalid"}`))
	} else {
		logCust.Type = "customer"
		json.NewEncoder(w).Encode(logCust)
	}
}

func custRegister( w http.ResponseWriter, req *http.Request) {
	db, err := sql.Open("postgres", os.Getenv("DATABASE_URL"))
	if(CheckError(err)){
		fmt.Println("Connected!")
	} else {
		fmt.Println(err)
	}
	defer db.Close()
	var cust Customer
	var user User
	_ = json.NewDecoder(req.Body).Decode(&cust)
	queryStmt := fmt.Sprint("select username, password from customers where username = '", cust.Username, "' and password = '", cust.Password, "'")
	regRow := db.QueryRow(queryStmt)
	err = regRow.Scan(&user.Username, &user.Password)
	if(cust.Username == user.Username) {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(`{"message": "Username has been taken."}`))
	} else {
		insertStmt := `insert into customers ("username", "password", "email", "name", "phone", "address") values($1,$2,$3,$4,$5,$6)`
		_, e := db.Exec(insertStmt,
			cust.Username,
			cust.Password,
			cust.Email,
			cust.Name,
			cust.Phone,
			cust.Address)
		if (e != nil){
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(`{"message": "Could not register customer."}`))
		} else {
			w.WriteHeader(http.StatusCreated)
			w.Write([]byte(`{"message": "New customer has been registered."}`))
		}
	}
}	

func getAllProducts( w http.ResponseWriter, req *http.Request) {
	db, err := sql.Open("postgres", os.Getenv("DATABASE_URL"))
	if(CheckError(err)){
		fmt.Println("Connected!")
	} else {
		fmt.Println(err)
	}
	defer db.Close()
	sqlStmt := fmt.Sprint("select product_id, vendor_id, product_name, description, variants, img0, img1, img2, aprice, sprice, quantity, unit from products")
	rows, e := db.Query(sqlStmt)
	defer rows.Close()
	if( e == sql.ErrNoRows) {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(`{"message": "Could not find any products from vendors."}`))
	} else {
		var items []Product
		for rows.Next() {
			var i Product
			err = rows.Scan(&i.ProductId,
				&i.VendorId,
				&i.Name,
				&i.Descr,
				&i.Variants,
				&i.Img0,
				&i.Img1,
				&i.Img2,
				&i.ActualPrice,
				&i.SellingPrice,
				&i.Quantity,
				&i.Unit)
			// if err != nil {
			// 	w.WriteHeader(http.StatusInternalServerError)
			// 	w.Write([]byte(`{"message": "Failure to read data from rows"}`))
			// 	json.NewEncoder(w).Encode(i)
			// 	return
			// }
			items = append(items, i)
		}
		json.NewEncoder(w).Encode(items)
	}
}

func buyProduct( w http.ResponseWriter, req *http.Request) {
	db, err := sql.Open("postgres", os.Getenv("DATABASE_URL"))
	if(CheckError(err)){
		fmt.Println("Connected!")
	} else {
		fmt.Println(err)
	}
	defer db.Close()
	var p Buyer
	var count int
	_ = json.NewDecoder(req.Body).Decode(&p)
	sqlStmt := fmt.Sprint("select count(*) from customers where cust_id = ", p.CID)
	err = db.QueryRow(sqlStmt).Scan(&count)
	if (err != nil) {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{"message": "Failed to find Customer Id."}`))
		return
	}
	insertStmt := `insert into orders ("product_name", "total", "vendor_id", "cust_id", "product_id") values($1,$2,$3,$4,$5)`
	_, e := db.Exec(insertStmt,
		p.Pname,
		p.Price,
		p.VID,
		p.CID,
		p.PID)
	if e != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{"message": "Failed count."}`))
		return
	} else {
		w.WriteHeader(http.StatusCreated)
		w.Write([]byte(`{"message": "Customer has placed order."}`))
	}
}

func getCustOrders( w http.ResponseWriter, req *http.Request) {
	db, err := sql.Open("postgres", os.Getenv("DATABASE_URL"))
	if(CheckError(err)){
		fmt.Println("Connected!")
	} else {
		fmt.Println(err)
	}
	defer db.Close()
	var c Id
	_ = json.NewDecoder(req.Body).Decode(&c)
	sqlStmt := fmt.Sprint("select order_id, product_name, total, busi_name from orders inner join vendors on orders.vendor_id = vendors.vendor_id where cust_id = ", c.Id)
	rows, e := db.Query(sqlStmt)
	defer rows.Close()
	if( e == sql.ErrNoRows) {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(`{"message": "Could not find any order for customer."}`))
	} else {
		var o []CustOrder
		for rows.Next() {
			var i CustOrder
			err = rows.Scan(&i.OrderId,
				&i.Pname,
				&i.Price,
				&i.Vname)
			// if err != nil {
			// 	w.WriteHeader(http.StatusInternalServerError)
			// 	w.Write([]byte(`{"message": "Failure to read data from rows"}`))
			// 	json.NewEncoder(w).Encode(i)
			// 	return
			// }
			log.Println(fmt.Sprint("order: ", i))
			o = append(o, i)
		}
		json.NewEncoder(w).Encode(o)
	}
}

func updateCustProfile( w http.ResponseWriter, req *http.Request) {
	db, err := sql.Open("postgres", os.Getenv("DATABASE_URL"))
	if(CheckError(err)){
		fmt.Println("Connected!")
	} else {
		fmt.Println(err)
	}
	defer db.Close()
	var curr UpdateCustomer
	var updated UpdateCustomer
	//	var dbUser User
	_ = json.NewDecoder(req.Body).Decode(&curr)
	// queryStmt := fmt.Sprint("select username, password from vendors where username = '", curr.CID, "'")
	// regRow := db.QueryRow(queryStmt)
	// err = regRow.Scan(&dbUser.Username, &dbUser.Password)
	// if(curr.Username == dbUser.Username) {
	// 	w.WriteHeader(http.StatusBadRequest)
	// 	w.Write([]byte(`{"message": "Username has been taken."}`))
	// 	return
	// }
	_, e := db.Exec(`UPDATE customers SET username=$1, password=$2, email=$3, name=$4, phone=$5, address=$6 WHERE cust_id=$7`,
		curr.Username,
		curr.Password,
		curr.Email,
		curr.Name,
		curr.Phone,
		curr.Address,
		curr.CID)
	if(CheckError(e)){
		updateProStmt := `SELECT cust_id, username, password, email, name, phone, address FROM customers where cust_id = $1`
		proRow := db.QueryRow(updateProStmt, curr.CID)
		err = proRow.Scan(&updated.CID,
			&updated.Username,
			&updated.Password,
			&updated.Email,
			&updated.Name,
			&updated.Phone,
			&updated.Address)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(`{"message": "Failed to obtain new profile."}`))
			return
		} else {
			json.NewEncoder(w).Encode(updated)
		}
	} else {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(`{"message": "Could not find customer profile."}`))
		return
	}
}

func getCustProfile( w http.ResponseWriter, req *http.Request) {
	db, err := sql.Open("postgres", os.Getenv("DATABASE_URL"))
	if(CheckError(err)){
		fmt.Println("Connected!")
	} else {
		fmt.Println(err)
	}
	defer db.Close()
	var c Id
	var cust UpdateCustomer
	_ = json.NewDecoder(req.Body).Decode(&c)
	proStmt := `SELECT cust_id, username, password, email, name, phone, address FROM customers WHERE cust_id = $1`
	proRow := db.QueryRow(proStmt, c.Id)
	err = proRow.Scan(&cust.CID,
		&cust.Username,
		&cust.Password,
		&cust.Email,
		&cust.Name,
		&cust.Phone,
		&cust.Address)
	if(err != nil) {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(`{"message": "Customer can not be found."}`))
	} else {
		json.NewEncoder(w).Encode(cust)
	}
}

func CheckError(err error) bool {
	if err != nil {
		return false
	} else {
		return true
	}
}

func main() {
	router := mux.NewRouter()
	fmt.Println("Starting the application...")
	//Vendor Routes
	router.HandleFunc("/login", loginToken).Methods("POST")
	router.HandleFunc("/register", registerVendor).Methods("POST")
	router.HandleFunc("/vendorprofile", validate( getVendorProfile)).Methods("POST")
	router.HandleFunc("/profileupdate", validate( updateVendorProfile)).Methods("POST")
	router.HandleFunc("/catalog", validate( getCatalog)).Methods("POST")
	router.HandleFunc("/product", validate( getProduct)).Methods("POST")
	router.HandleFunc("/addproduct", validate( addProduct)).Methods("POST")
	router.HandleFunc("/updateproduct", validate( updateProduct)).Methods("POST")
	router.HandleFunc("/removeproduct", validate( removeProduct)).Methods("POST")
	router.HandleFunc("/orders", validate( getOrders)).Methods("POST")
	//Customer Routes
	router.HandleFunc("/custlogin", custLogin).Methods("POST")
	router.HandleFunc("/custreg", custRegister).Methods("POST")
	router.HandleFunc("/custprofile", getCustProfile).Methods("POST")
	router.HandleFunc("/updatecust", updateCustProfile).Methods("POST")
	router.HandleFunc("/allproducts", getAllProducts).Methods("GET")
	router.HandleFunc("/buy", buyProduct).Methods("POST")
	router.HandleFunc("/custorders", getCustOrders).Methods("POST")
	port := os.Getenv("PORT")
	if port == "" {
		port = "8000" //localhost
	}

	log.Fatal(http.ListenAndServe(":"+port,router))
}
