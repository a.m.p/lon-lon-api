# Lon Lon API

# API for Product catalog app: Vendorise

+  Adds Vendor
+  Gets vendor profile, orders, catalog, product information.
+  Deletes photos from Bucket
+  Adds Customer
+  Gets User orders, Vendor product catalog, Customer profile.

Note from a.m.p. : This is beta, do not use on production. Built to showcase Vendorise application.
