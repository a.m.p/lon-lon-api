module lon-lon-api

go 1.15

require (
	github.com/aws/aws-sdk-go v1.38.34
	github.com/aws/aws-sdk-go-v2 v1.3.4 // indirect
	github.com/aws/aws-sdk-go-v2/config v1.1.6 // indirect
	github.com/aws/aws-sdk-go-v2/service/s3 v1.5.0 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/context v1.1.1
	github.com/gorilla/mux v1.8.0
	github.com/jmoiron/sqlx v1.3.3
	github.com/lib/pq v1.10.0
	github.com/mitchellh/mapstructure v1.4.1
)
